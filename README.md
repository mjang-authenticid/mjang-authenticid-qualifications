# Mike Jang's Qualifications

Mike Jang's qualifications for the Authentic ID Technical Writer position.

- Resume: mjangResume.md
- Writing Samples: listOfWritingSamples.md
- Cover Letter: coverLetter.md
- Speaking Experience: speakingExperience.md

